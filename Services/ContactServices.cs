using System.Collections.Generic;
using System.Linq;
using examen.Context;
using Examen.Models;

namespace examen.Services
{
    public class ContactServices
    {
        public ApplicationDbContext db { get; set; }

        public ContactServices(ApplicationDbContext context)
        {
            db = context;
        }
        public void AddContact( Contacts contact)
        {
            db.Contacts.Add(contact);
            db.SaveChanges();
        }

        public List<Contacts> GetUserContacts(int id)
        {
            return db.Contacts.Where(x => x.UserId == id).ToList();
        }

        public Contacts GetContact(string email)
        {
            return db.Contacts.FirstOrDefault(x => x.Email == email);
        }

    }
}