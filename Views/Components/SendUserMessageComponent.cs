using System;
using examen.Services;
using examen.States;
using examen.Views;
using Examen.Models;
namespace examen2.Views.Components
{
    public class SendUserMessageComponent
    {
        public UserServices UserServices { get; }
        private readonly UserServices userServices;
        private readonly MessageServices messageServices;
        private readonly ContactServices contactServices;

        public SendUserMessageComponent(UserServices userServices, MessageServices messageServices, ContactServices contactServices)
        {
            this.userServices = userServices;
            this.messageServices = messageServices;
            this.contactServices = contactServices;
        }

        public void OnInit()
        {
            bool estado = false;
            do
            {
                this.PrintContacts();
                Console.WriteLine("");
                this.SendMessage();
                Console.Write("\n\t\t[S] si [N] Any\n\t\tDesea agregar otro usuario?: ");
                if (Console.ReadLine().ToUpper()[0] == 'S')
                {
                    Console.Clear();
                    estado = true;
                }
                else
                {
                    Console.Clear();
                    estado = false;
                }
            } while (estado);
        }

        public void PrintContacts()
        {
            var contacts = this.contactServices.GetUserContacts(ApplicationState.IdUserLogged);
            if (contacts.Count != 0)
            {
                Console.WriteLine("\n\t\tId\t\tCorreo");
                foreach (var contact in contacts)
                {
                    Console.WriteLine($"\n\t\t{contact.Id}\t\t{contact.Email}");
                }
            }
            else
            {
                Console.WriteLine("\n\t\tNo hay ningun Contacto registrado");
            }
        }

        private void SendMessage()
        {
            Message message = new Message();
            Console.WriteLine($"\n\t\tDe: {this.userServices.GetUserById(ApplicationState.IdUserLogged).Email}");
            Console.Write("\t\tPara: ");
            string email = Console.ReadLine();
            Console.Write("\n\t\tAsunto: ");
            string asunto = Console.ReadLine();
            Console.Write("\n\t\tMensaje: ");
            string mensage = Console.ReadLine();

            var contact = this.contactServices.GetContact(email);

            if (contact == null)
            {
                this.contactServices.AddContact(new Contacts() { Email = email, UserId =  ApplicationState.IdUserLogged});
                contact = this.contactServices.GetContact(email);
            }
            else
            {
                message.ContactId = contact.Id;
                message.UserId = ApplicationState.IdUserLogged;
                message.Affair = asunto;
                message.MessageUser = mensage;
                message.ContactId = contact.Id;
                message.State = true;

                this.messageServices.AddMessage(message);
            }
        }
    }
}