using System;
using examen.Services;
using examen.States;
using Examen.Models;

namespace examen2.Views.Components
{
    public class LoginUserComponent
    {
        public LoginUserComponent(UserServices userServices)
        {
            UserServices = userServices;
        }

        public UserServices UserServices { get; }

        public void OnInit()
        {
            bool State = false;
            do
            {
                State = this.FindUser();
                if(!State)
                {
                    Console.Clear();
                    Console.WriteLine("\t\tUsuario y/o contrasena incorrectos.\n\t\tFavor ingresarlos de nuevo");
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("\t\tBienvenido al Sistema");
                }
            }while(false);
        }

        private User GetDataUser()
        {
            Console.Write("\n\t\tEmail: ");
            string email = Console.ReadLine();
            Console.Write("\t\tEmail: ");
            string password = Console.ReadLine();
            return new User
            {
                Email = email,
                Password = password
            };
        }

        public bool FindUser()
        {
            var data = this.GetDataUser();
            var user = this.UserServices.GetUserByData(data.Email, data.Password);

            if(user != null)
            {
                ApplicationState.IdUserLogged = user.Id;
                return false;
            }else
            {
                return true;
            }
        }
    }
}