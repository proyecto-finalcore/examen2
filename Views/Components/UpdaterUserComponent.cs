using System;
using examen.Context;
using examen.Services;
using Examen.Models;

namespace examen.Views.Components
{
    public class UpdaterUserComponent
    {
        public UserServices UserServices { get; }

        public UpdaterUserComponent(UserServices userServices)
        {
            UserServices = userServices;
        }
        public void OnInit()
        {
            bool estado = false;
            do
            {
                Console.WriteLine("\n\n\t\t---- Actualizar Usuario ----\n\n");
                var user = this.GetUserToModifie();
                this.UpdaterDataUser(user);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\t\tUsuarios actualizado exitosamente");
                Console.ResetColor();
                Console.Write("\n\t\t[S] si [N] Any\n\t\tDesea actualizar otro usuario?: ");
                if (Console.ReadLine().ToUpper()[0] == 'S')
                {
                    Console.Clear();
                    estado = true;
                }
                else
                {
                    Console.Clear();
                    estado = false;
                }
            } while (estado);
        }

        private User GetUserToModifie()
        {
            Console.Write("\n\t\tId de usuario: ");
            var user = this.UserServices.GetUserById(int.Parse(Console.ReadLine()));

            return user;
        }

        private void UpdaterDataUser(User user)
        {
            try
            {
                Console.Write("\n\t\tNickname: ");
                user.Nickname = Console.ReadLine();

                Console.Write("\t\tEmail: ");
                user.Email = Console.ReadLine();

                Console.Write("\t\tPassword: ");
                user.Password = Console.ReadLine();

                this.UserServices.UpdateUser(user);
            }
            catch (Exception e)
            {
                Partials.HandleException(e,"Error al actualizar el usuario");
            }
        }
    }
}