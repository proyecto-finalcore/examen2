using System;
using examen.Services;
using Examen.Models;
namespace examen.Views.Components
{
    public class RegisterUserComponent
    {
        public RegisterUserComponent(UserServices userServices)
        {
            UserServices = userServices;
        }

        public UserServices UserServices { get; }

        public void OnInit()
        {
            bool estado = false;
            do
            {
                Console.WriteLine("\n\n\t\t---- Registrar Usuario ----\n\n");
                this.GetDataUser();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\t\tUsuarios agregado exitosamente");
                Console.ResetColor();
                Console.Write("\n\t\t[S] si [N] Any\n\t\tDesea agregar otro usuario?: ");
                if (Console.ReadLine().ToUpper()[0] == 'S')
                {
                    Console.Clear();
                    estado = true;
                }else
                {
                    Console.Clear();
                    estado = false;
                }
            } while (estado);
        }

        private void GetDataUser()
        {
            User user = new User();

            try
            {
                Console.Write("\n\t\tNickname: ");
                user.Nickname = Console.ReadLine();

                Console.Write("\t\tEmail: ");
                user.Email = Console.ReadLine();

                Console.Write("\t\tPassword: ");
                user.Password = Console.ReadLine();

                if ((user.Nickname != "" || user.Nickname != null) || (user.Email != "" || user.Email != null) || (user.Password != "" || user.Password != null))
                {
                    this.UserServices.AddUser(user);
                }
                else
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("\t\tNo pueden haber campos nulos");
                    Console.ResetColor();
                    this.GetDataUser();
                }
            }
            catch (Exception e)
            {
                Partials.HandleException(e, "Error al ingresar usuario");
            }

        }
    }
}