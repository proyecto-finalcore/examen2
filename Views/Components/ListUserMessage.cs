using System;
using examen.Services;
using examen.States;
namespace examen2.Views.Components
{
    public class ListUserMessage
    {
        public ListUserMessage(MessageServices messageServices)
        {
            MessageServices = messageServices;
        }

        public MessageServices MessageServices { get; }

        public void OnInit()
        {
            bool State = false;
            do
            {
                this.PrintMessage();
                int messajeId = this.ReadMessaje();
                var message = this.MessageServices.GetMessageById(messajeId);
                Console.WriteLine($"\n\t\tID: {message.Id}\n\t\tAsunto: {message.Affair}\n\t\tMensaje:{message.MessageUser.Substring(0, 10)}...");
                Console.Write("\n\t\t[S] si [N] Any\n\t\tDesea actualizar otro usuario?: ");

                if (Console.ReadLine().ToUpper()[0] == 'S')
                {
                    Console.Clear();
                    State = true;
                }
                else
                {
                    Console.Clear();
                    State = false;
                }
            } while (State);
        }

        public void PrintMessage()
        {
            var Messages = this.MessageServices.GetUserSendedMessage(ApplicationState.IdUserLogged);
            Console.WriteLine($"\n\t\tId\t\tPara:\t\tAsunto:\t\tMensaje:");
            foreach (var message in Messages)
            {
                Console.WriteLine($"\n\t\t{message.Id}\t\t{message.Affair}\t\t{message.MessageUser}");
            }

        }

        public int ReadMessaje()
        {
            Console.Write("\n\t\tSeleccione un mensaje: ");
            return int.Parse(Console.ReadLine());
        }

    }
}