using System;
using examen.Services;
using examen2.Views.Components;

namespace examen2.Views.Pages
{
    public class SistemPage
    {
        public LoginUserComponent loginUserComponent { get; set; }
        public SendUserMessageComponent sendUserMessageComponent { get; set; }
        public ListUserMessage listUserMessage { get; set; }
        public SistemPage(UserServices userServices, MessageServices messageServices, ContactServices contactServices)
        {
            this.loginUserComponent = new LoginUserComponent(userServices);
            this.sendUserMessageComponent = new SendUserMessageComponent(userServices,messageServices,contactServices);
            this.listUserMessage = new ListUserMessage(messageServices);
        }

        public void OnInit()
        {
            Console.WriteLine("\n\t\t--- Login ---");
            var stado = this.loginUserComponent.FindUser();
            if (!stado)
            {
                this.PrintMenu();
                var option =  this.GetOption();
                this.BuildMenu(option);
            }else
            {
                Console.Clear();
                this.OnInit();
            }

        }
        public void PrintMenu()
        {
            Console.WriteLine("\n\t\t1 - Enviar Correo\n\t\t2 - Listado de Correos\n\t\t3 - Cerrar");
        }

        private int GetOption()
        {
            Console.Write("\t\tSeleccione una opcion: ");
            return int.Parse(Console.ReadLine());
        }
        private void BuildMenu(int option)
        {
            switch (option)
            {
                case 1:
                    this.sendUserMessageComponent.OnInit();
                    break;
                case 2:
                    this.listUserMessage.OnInit();
                    break;
                case 3:
                    break;
                default:
                    this.OnInit();
                    break;
            }
        }
    }
}