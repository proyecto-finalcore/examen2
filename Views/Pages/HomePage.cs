using System;
using examen.Services;
using examen2.Views.Pages;
namespace examen.Views.Pages
{
    public class HomePage
    {
        private AdminPage adminPage { get; set; }
        private SistemPage sistemPage { get; set; }
        public HomePage(UserServices userServices, MessageServices messageServices, ContactServices contactServices)
        {
            this.adminPage = new AdminPage(userServices);
            this.sistemPage = new SistemPage(userServices,messageServices, contactServices);
        }
        public void OnInit()
        {
            PrintMenu();
            int option = this.GetOption();
            this.BuildMenu(option);
        }

        public int GetOption()
        {
            Console.Write("\n\t\tSelecciona una opcion: ");
            return int.Parse(Console.ReadLine());
        }

        private void PrintMenu()
        {
            Console.WriteLine("\t\t1 - Admin\n\t\t2 - Sistema\n\t\t3 - Salir");
        }
        public void BuildMenu(int option)
        {
            Console.Clear();
            switch (option)
            {
                case 1:
                    this.adminPage.OnInit();
                    Console.Clear();
                    this.OnInit();
                break;
                case 2:
                    this.sistemPage.OnInit();
                     Console.Clear();
                     this.OnInit();
                break;
                case 3:

                break;
                default:
                    Console.WriteLine("\t\tLa opcion que selecciono es inexistente");
                    this.OnInit();
                break;
            }
        }
    }
}