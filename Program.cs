﻿using System.Runtime.CompilerServices;
using System;
using examen.Services;
using examen.Views.Pages;
using examen.Context;

namespace examen
{

    class Program
    {
        
        static void Main(string[] args)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            UserServices userServices = new UserServices(context);
            MessageServices messageServices = new MessageServices(context);
            ContactServices contactServices = new ContactServices(context);
            HomePage homePage = new HomePage(userServices,messageServices, contactServices);


            homePage.OnInit();
        }
    }
}
